import { Component, OnInit } from '@angular/core';
import { Account } from '../../../class/account';
import { AccountInsertComponent } from '../../dialog/account-insert/account-insert.component';
import { ResponseApi } from 'src/app/class/response-api';
import { MatSnackBar, MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AccountService } from 'src/app/service/account.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  displayedColumns: string[] = ['accountId', 'name', 'Action'];

  dataSource: MatTableDataSource<Account> = new MatTableDataSource();

  constructor(private accountService: AccountService, private dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.paging();
  }
  paging() {
    this.accountService.paging().subscribe((res: ResponseApi) => {
      this.dataSource.data = res.data;

    });
  }

  insert() {
    this.dialog.open(AccountInsertComponent, {
      width: '600px',
      data: { parent: this }
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  delete(accountId: number) {
    if (confirm('Bạn chọn xóa nhân viên. Bạn muốn tiếp tục?')) {
      this.accountService.delete(accountId).subscribe((res: ResponseApi) => {
        this.paging();
        if (res.success) {
          this.snackBar.open(res.message, 'Đóng', {
            panelClass: ['style-success'],
            duration: 2500
          });
        } else {
          this.snackBar.open(res.message, 'Đóng', {
            panelClass: ['style-error'],
            duration: 2500
          });
        }
      });
    }
  }
  edit(accountSelected: any) {
    this.dialog.open(AccountInsertComponent, {
      width: '600px',
      data: { parent: this, invoi: accountSelected }
    });
  }

}
