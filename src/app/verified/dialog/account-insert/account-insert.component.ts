import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Md5 } from 'ts-md5';
import { ResponseApi } from 'src/app/class/response-api';
import { Account } from 'src/app/class/account';
import { AccountService } from 'src/app/service/account.service';
import { AccountComponent } from 'src/app/verified/pages/account/account.component'

@Component({
  selector: 'app-account-insert',
  templateUrl: './account-insert.component.html',
  styleUrls: ['./account-insert.component.scss']
})
export class AccountInsertComponent implements OnInit {
  account: Account = new Account();
  isEdit: boolean;
  form: FormGroup;
  get accountId() {
    return this.form.get('accountId');
  }
  get name() {
    return this.form.get('name');
  }
  constructor(private accountService:AccountService,public dialogRef: MatDialogRef<AccountInsertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private snackBar: MatSnackBar) { }

    ngOnInit() {

      if (this.data['account']) {
        this.account = this.data['account'];
        this.isEdit = true;
        this.form = new FormGroup({
          name: new FormControl(this.account.name, [Validators.required]),
        });
      } else {
        this.form = new FormGroup({
          name: new FormControl(this.account.name, [Validators.required]),
        });
      }
  }
  getErrorMessage(input: any) {
    return 'Vui lòng nhập trường này';
  }
  onSubmit(form: FormGroup): void {
    if (form.invalid) {
      return;
    }
    const formData = this.form.value;
    if (this.isEdit) {
      this.accountService.edit(formData).subscribe((res: ResponseApi) => {
        const parent: AccountComponent = this.data['parent'] as AccountComponent;
        if (res.success) {
          this.snackBar.open(res.message, 'Đóng', {
            panelClass: ['style-success'],
            duration: 2500
          });
        } else {
          this.snackBar.open(res.message, 'Đóng', {
            panelClass: ['style-error'],
            duration: 2500
          });
        }
        parent.paging();
      });
    } else {
      if (formData['password']) {
        formData['password'] = new Md5().appendStr(formData['password']).end();
      }
      this.accountService.insert(formData).subscribe((res: ResponseApi) => {
        const parent: AccountComponent = this.data['parent'] as AccountComponent;
        if (res.success) {
          this.snackBar.open(res["message"], 'Đóng', {
            panelClass: ['style-success'],
            duration: 2500
          });
        } else {
          this.snackBar.open(res["message"], 'Đóng', {
            panelClass: ['style-error'],
            duration: 2500
          });
        }
        parent.paging();
      });
    }
  }
}
