import { Injectable } from '@angular/core';
import{HttpClient, HttpParams} from '@angular/common/http'
import{Account} from '../class/account'

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = `${localStorage.getItem('ServerUrl')}/Account/ListAccount`;
  constructor(private http: HttpClient) { }

  public paging() {
    return this.http.get(`${this.baseUrl}`);
  }
  public insert(admin: Account) {
    return this.http.post(`${this.baseUrl}/Insert`, admin);
  }
  public edit(admin: Account){
    return this.http.post(`${this.baseUrl}/Edit`, admin);
  }
  public delete(id: Number){
    return this.http.delete(`${this.baseUrl}/Delete/${id}`);
  }
  public findAllAvaiable() {
    return this.http.get(`${this.baseUrl}/FindAllAvaiable`);
  }
  public templateImporting() {
    return this.http.get(`${this.baseUrl}/ImportTemplate`, { responseType: 'blob' });
  }
  public importAdmins(data: any) {
    return this.http.post(`${this.baseUrl}/Import`, data);
  }
  public findAllInDepartment(id) {
    return this.http.get(`${this.baseUrl}/FindAllInDepartment/${id}`);
  }
  public findAllInSalaryTableProduct(id) {
    return this.http.get(`${this.baseUrl}/FindAllInSalaryTableProduct/${id}`);
  }
  public changePassword(params: any) {
    let httpParams = new HttpParams();
    if (params) {
      Object.keys(params).forEach(key => {
        httpParams = httpParams.append(key, params[key]);
      });
    }
    return this.http.post(`${this.baseUrl}/changePassword`, httpParams);
  }
  public getInformation() {
    return this.http.get(`${this.baseUrl}/Information`);
  }
  public findAdminInRole() {
    return this.http.get(`${this.baseUrl}/FindAdminInRole`);
  }
}
